package com.solutions.ica.androidtvnativesdkpoc;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.loadIcon();
        this.setIP();
        this.configureButton();

    }

    private void loadIcon(){
        ImageView logoView = (ImageView) findViewById(R.id.logoView);
        Picasso.get().load("http://brandmark.io/logo-rank/random/apple.png").into(logoView);
    }

    public void setTitle(String title){
        TextView ip = (TextView) findViewById(R.id.ipTextView);
        ip.setText(title);
    }

    private void configureButton(){
        Button clickButton = (Button) findViewById(R.id.playVideoButton);
        clickButton.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(v.getContext(), PlayVideoActivity.class);
                v.getContext().startActivity(myIntent);
            }
        });
    }
    private void setIP() {
        String url = "https://ipinfo.io/";

        RequestQueue queue = Volley.newRequestQueue(this);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response)  {
                        try {
                            setTitle("IP( " + response.getString("ip") + " )");
                        } catch (JSONException e) {
                            e.printStackTrace();
                            setTitle("IP: Not Found");
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        setTitle("Welcome to Android TV !"); // Badge, when set, takes preceden over title
                    }

                }) {/**
         * Passing some request headers
         */
        @Override
        public Map<String, String> getHeaders () throws AuthFailureError {
            HashMap<String, String> headers = new HashMap<String, String>();
            headers.put("Content-Type", "application/json");
            headers.put("Accept", "application/json");
            return headers;
        }};

        queue.add(jsonObjectRequest);
    }

}
