package com.solutions.ica.androidtvnativesdkpoc;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.MediaController;
import android.widget.Toast;

public class BackMediaController extends MediaController {

    ImageButton mCCBtn;
    Context mContext;
    AlertDialog mLangDialog;

    public BackMediaController(Context context) {
        super(context);
        mContext = context;
    }

    @Override
    public void setAnchorView(View view) {
        super.setAnchorView(view);

        FrameLayout.LayoutParams frameParams = new FrameLayout.LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        frameParams.gravity = Gravity.RIGHT|Gravity.TOP;

        View v = makeCCView();
        addView(v, frameParams);

    }

    private View makeCCView() {
        mCCBtn = new ImageButton(mContext);
        mCCBtn.setImageResource(R.drawable.arrow_right_3);

        mCCBtn.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                Intent myIntent = new Intent(v.getContext(), MainActivity.class);
                v.getContext().startActivity(myIntent);
            }
        });

        return mCCBtn;
    }
}
